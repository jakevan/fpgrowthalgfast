# Fp Growth Algorithm Program:

# Compilation
 - Already compiled against Java 14. (The program runs about 2x slower for me if compiled with java 8)
 - If not, `javac *`

# Running
 - In the main directory:
 - java Fpgrowth <filename> <minsup pct>
 For example, java Fpgrowth connect.txt 50

 - There is also a java 8 version of the program, called Java8Files.jar.
 - To run: java -jar Java8Files.jar connect.txt 50