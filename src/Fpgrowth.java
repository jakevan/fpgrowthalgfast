import utils.IntPair;
import utils.Pair;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class Fpgrowth {
    public static int minsup = 2;
    /**
     * Basic structure: Construct Data Table from file.
     * Construct FreqTable from that data table.
     * Sort the data table by the frequency table.
     * Construct the first TreeLinkTable (TLT) via these tables.
     * Recursively construct projected TLTs via this main table.
     */
    public static void main(String[] args) {
        if(args.length == 2){
            String fname = args[0];
            int minsupPct = Integer.parseInt(args[1]);
            System.out.println("Parsing data from " + fname + " with minsup pct: " + minsupPct);
            long startTime = System.currentTimeMillis();
            DatasetTable table = DatasetTable.constructFromFile(fname);
            minsup = (int) (table.transactionCount*(minsupPct/100.0));
            System.out.println("Minimum Support threshold: " + minsup);
            FrequencyTable frequencyTable = FrequencyTable.constructFromDataset(table, minsup);
            table.sortByFreqTable(frequencyTable);
            TreeLinkTable treeLinkTable = TreeLinkTable.constructFromSortedDatasetTableAndFrequencyTable(table, frequencyTable);
            System.out.println("Finished constructing from file in: " + (System.currentTimeMillis()-startTime)/1000.0 + "s");
            findFreqPatterns(treeLinkTable, new ArrayList<>());
            System.out.println("|FPs| = " + freq.size());
            double runtime = (System.currentTimeMillis()-startTime)/1000.0;
            System.out.println("Total Runtime: " + new DecimalFormat("#0.000").format(runtime) + "s");
            try {
                FileWriter writer = new FileWriter("MiningResult.txt");
                writer.write(getFreqStr());
                System.out.println("Finished writing to MiningResult.txt");
                writer.close();
            } catch (IOException e) {
                System.out.println("Failed to write to file");
                throw new RuntimeException(e);
            }
//            System.out.println(getFreqStr());
        }else{
            System.out.println("Usage: java Fpgrowth <filename> <minsup %>");
        }

    }
    // The last element in the frequency array will be the count.
    public static ArrayList<Pair<Integer, ArrayList<Integer>>> freq = new ArrayList<>();

    // Sort and print the output so it looks like the sample output
    public static String getFreqStr(){
        // First, sort every individual FP
        for (Pair<Integer, ArrayList<Integer>> is : freq) {
            is.v.sort(Integer::compare);
        }
        // Sort all FPs
        freq.sort(new Comparator<Pair<Integer, ArrayList<Integer>>>() {
                      @Override
                      public int compare(Pair<Integer, ArrayList<Integer>> o1P, Pair<Integer, ArrayList<Integer>> o2P) {
                          ArrayList<Integer> o1 = o1P.v;
                          ArrayList<Integer> o2 = o2P.v;
                          // The shorter list takes priority
                          if(o1.size() > o2.size()){
                              return 1;
                          }else if(o1.size() < o2.size()){
                              return -1;
                          }
                          // If same size, sort by elements
                          int minSize = o1.size();
                          for (int i = 0; i < minSize; i++) {
                              int a = o1.get(i);
                              int b = o2.get(i);
                              if(a == b) continue;
                              else if(a > b) return 1;
                              else return -1;
                          }

                          return 0;
                      }
                  }
        );


        // Lastly, get a string of everything
        StringBuilder sb = new StringBuilder("|FPs| = ");
        sb.append(freq.size());
        sb.append("\n");
        for (Pair<Integer, ArrayList<Integer>> ips : freq) {
            ArrayList<Integer> integers = ips.v;
            int size = integers.size();
            for (int i = 0; i < size - 1; i++) {
                sb.append(integers.get(i)).append(", ");
            }
            sb.append(integers.get(size - 1));
            sb.append(" : ");
            sb.append(ips.k);
            sb.append("\n");
        }
        return sb.toString();
    }
    public static void findFreqPatterns(TreeLinkTable tlt, ArrayList<Integer> tree){
        // Print out frequent patterns
        for (IntPair intPair : tlt.getHeaderSorted()) {
            ArrayList<Integer> t = new ArrayList<>(tree);
            t.add(intPair.k);
            freq.add(new Pair<>(intPair.v, t));
            // System.out.println(tree + " " + intPair.k + "] : " + intPair.v);
        }

        // Construct all the projected patterns
        for (int i = tlt.getHeaderSorted().size() - 1; i >= 0; i--) {
            IntPair entry = tlt.getHeaderSorted().get(i);
            TreeLinkTable tlt2 = TreeLinkTable.constructProjTree(tlt.getHeaderLinkTable().get(entry.k), minsup);
            ArrayList<Integer> tr = new ArrayList<>(tree);
            tr.add(entry.k);
            findFreqPatterns(tlt2, tr);
        }
        // Base case: No FP's. Handled by the fact that tlt.getHeaderSize() == 0

    }
}