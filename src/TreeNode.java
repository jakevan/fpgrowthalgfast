import java.util.ArrayList;
import java.util.HashMap;

public class TreeNode {
    public int id = -1;
    public int count;

    // Item ID to treenode lookup
    public HashMap<Integer, TreeNode> nodeLinks = new HashMap<>();

    // Node 1 up in the tree
    public TreeNode upNode;

}
