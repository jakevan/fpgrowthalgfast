import utils.IntPair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * The main data type, contains a hyperlink header table and a tree.
 */
public class TreeLinkTable {
    // Instead of having a header datatype, we have 2 fast lookups for count and hyperlinks
    // Then the sorted type -> header array that's sorted.

    // Sorted header table
    private ArrayList<IntPair> headerSorted = new ArrayList<>();
    // Header count lookup
    private HashMap<Integer, Integer> headerCountTable = new HashMap<>();

    // Header hyperlink lookup. In the notes its similar a linked list, but arraylist is easier to construct
    private HashMap<Integer, ArrayList<TreeNode>> headerLinkTable = new HashMap<>();

    public HashMap<Integer, ArrayList<TreeNode>> getHeaderLinkTable() {
        return headerLinkTable;
    }

    public ArrayList<IntPair> getHeaderSorted() {
        return headerSorted;
    }

    private TreeNode rootNode = new TreeNode();

    /**
     * Constructs a projection tree based on another TreeLinkTable and its hyperlinks header.
     * The other TLT is not passed to the function cause the hyperlinks are enough (they are references to the tree)
     */

    public static TreeLinkTable constructProjTree(ArrayList<TreeNode> hyperLinks, int minsup){
        TreeLinkTable tlt = new TreeLinkTable();
        // First thing, we construct the tree structure of the new TreeLinkTable.
        // And we also populate the headerCountTable and headerLinkTable in here because it's convenient
        for (TreeNode link : hyperLinks) {
            int toAddCount = link.count;
            ArrayList<TreeNode> upNodes = new ArrayList<>();
            TreeNode cNode = link;
            while (true){
                // Follow link up
                if(cNode.upNode == null || cNode.upNode.id == -1){ // 0 is the root node
                    break;
                }
                upNodes.add(cNode.upNode);
                cNode = cNode.upNode;
            }

            // Now that we have the array of nodes, iterate downward and insert them to the tree
            TreeNode currentNode = tlt.rootNode;
            for (int i = upNodes.size()-1; i >= 0; i--) {
                TreeNode node = upNodes.get(i);
                // Insert top node intro tree
                TreeNode lnk = currentNode.nodeLinks.get(node.id);
                if(lnk == null){
                    TreeNode nNode = new TreeNode();
                    nNode.upNode = currentNode;
                    nNode.count = toAddCount; // 1
                    nNode.id = node.id;
                    currentNode.nodeLinks.put(nNode.id, nNode);
                    currentNode = nNode;

                    // For header table, insert into the count table
                    int count = tlt.headerCountTable.computeIfAbsent(currentNode.id, k -> 0);
                    count += nNode.count;
                    tlt.headerCountTable.put(currentNode.id, count);

                    // And construct the header link table
                    ArrayList<TreeNode> links = tlt.headerLinkTable.computeIfAbsent(currentNode.id, ArrayList::new);
                    links.add(nNode);

                }else{
                    lnk.count += toAddCount; // 1
                    currentNode = lnk;
                }
            }
        }

        // Ok, now that we have constructed the array, we need to generate the header table
        for (Map.Entry<Integer, Integer> entry : tlt.headerCountTable.entrySet()) {
            int id = entry.getKey();
            int cnt = entry.getValue();
            tlt.headerSorted.add(new IntPair(id, cnt));
        }

        // Clean up the headerSorted table to remove minsup items, keep them in the tree though cause we need it for the next projected tree
        ArrayList<IntPair> toRemove = new ArrayList<>();
        for (IntPair p : tlt.headerSorted) {
            if(p.v < minsup){
                toRemove.add(p);
            }
        }
        tlt.headerSorted.removeAll(toRemove);

        // And lastly, sort it.
        tlt.headerSorted.sort(new Comparator<IntPair>() {
            @Override
            public int compare(IntPair o1, IntPair o2) {
                // Compare by value
                int comp = Integer.compare(o2.v, o1.v);
                if(comp == 0){
                    // If values are equal, Compare by key
                    return Integer.compare(o1.k, o2.k);
                }
                return comp;
            }
        });


        return tlt;
    }

    // TLT's can be constructed from 2 places, a dataset+frequency table, or another tree link table (projected tree)
    // This is only used in the very first iteration because we generate the TLT from the dataset, not another TLT.
    public static TreeLinkTable constructFromSortedDatasetTableAndFrequencyTable(DatasetTable dt, FrequencyTable ft) {
        TreeLinkTable tlt = new TreeLinkTable();
        tlt.headerCountTable = ft.getCountTable(); // TLT Header Table (HT) = FT count table
        tlt.headerSorted = ft.getSortedTable(); // TLTHT = FT HT

        // Keep track of all of the nodes that need to be hyperlinked

        // Loop through every transaction in order
        for (int i = 1; i <= dt.transactionCount; i++) {
            ArrayList<Integer> items = dt.getTable().get(i);
            TreeNode currentTreeNode = tlt.rootNode;
            for (Integer item : items) {
                // For our current tree node, check if there is an existing node with the id.
                TreeNode nextNode = currentTreeNode.nodeLinks.get(item);
                if(nextNode == null){
                    // Need to make a new node
                    TreeNode node = new TreeNode();
                    node.upNode = currentTreeNode;
                    node.id = item;
                    node.count = 1;
                    currentTreeNode.nodeLinks.put(item, node);
                    // Switch to new node for the next iterations
                    currentTreeNode = node;

                    ArrayList<TreeNode> hlNodes = tlt.headerLinkTable.computeIfAbsent(item, k -> new ArrayList<>());
                    hlNodes.add(node);

                }else{
                    // Already exists in tree, just inc count by 1
                    currentTreeNode = nextNode;
                    currentTreeNode.count += 1;
                }
            }
        }

        return tlt;
    }
}
