package utils;

/**
 * Helper class, a pair of ints
 */
public class IntPair {
    public int k, v;

    public IntPair(int k, int v) {
        this.k = k;
        this.v = v;
    }
}
