import utils.IntPair;

import java.util.*;

/**
 * A frequency table. This is ONLY the frequency table for the initial:
 * Data file --> Frequency Table --> Sorted Dataset --> TreeLinkTable.
 * TreeLinkTables have their own frequency table built in.
 */
public class FrequencyTable {
    // We keep the data in 2 formats, IntPair for ordered data. countTable for fast count lookups.
    private ArrayList<IntPair> sorted = new ArrayList<>();
    private HashMap<Integer, Integer> countTable = new HashMap<>();
    // Also, we keep track of minsup removed items.


    public HashMap<Integer, Integer> getCountTable() {
        return countTable;
    }

    public ArrayList<IntPair> getSortedTable() {
        return sorted;
    }

    /**
     * Constructs a frequency table from a dataset.
     * Also, removes minsup elements from the dataset table
     */
    public static FrequencyTable constructFromDataset(DatasetTable table, int minsup){
        FrequencyTable ft = new FrequencyTable();
        for (Map.Entry<Integer, ArrayList<Integer>> entry : table.getTable().entrySet()) {
            Integer transactionId = entry.getKey();
            ArrayList<Integer> items = entry.getValue();
            for (Integer item : items) {
                Integer count = ft.countTable.get(item);
                if(count == null) count = 0;
                count++;
                ft.countTable.put(item, count);
            }
        }
        ArrayList<Integer> minSupRemoved = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : ft.countTable.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if(value < minsup) {
                // Under minsup!
                minSupRemoved.add(key);
                continue;
            }
            ft.sorted.add(new IntPair(key, value));
        }
        for (Integer i : minSupRemoved) {
            ft.countTable.remove(i);
        }

        ft.sorted.sort(new Comparator<IntPair>() {
            @Override
            public int compare(IntPair o1, IntPair o2) {
                int comp = Integer.compare(o2.v, o1.v);
                if(comp == 0){
                    // Compare by key
                    return Integer.compare(o1.k, o2.k);
                }
                return comp;
            }
        });

        // Remove minsup entries from the dataset table.
        for (ArrayList<Integer> entry : table.getTable().values()) {
            for (Integer msRemoved : minSupRemoved) {
                entry.remove(msRemoved);
            }
        }

        return ft;
    }
}
