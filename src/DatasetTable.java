import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class DatasetTable {
    public int transactionCount = 0;
    private HashMap<Integer, ArrayList<Integer>> table = new HashMap<>();

    public HashMap<Integer, ArrayList<Integer>> getTable() {
        return table;
    }

    /**
     * Sort the table
     */
    public void sortByFreqTable(FrequencyTable ft) {
        for (ArrayList<Integer> value : table.values()) {
            value.sort(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    Integer o1Freq = ft.getCountTable().get(o1);
                    Integer o2Freq = ft.getCountTable().get(o2);
                    if (Objects.equals(o1Freq, o2Freq)) {
                        // Sort by numeric value, if the count is equal
                        return Integer.compare(o1, o2);
                    }
                    return Integer.compare(o2Freq, o1Freq);
                }
            });
        }
    }

    public static DatasetTable constructFromFile(String fname) {
        DatasetTable table = new DatasetTable();
        try {
            File f = new File(fname);
            Scanner scanner = new Scanner(f);
            table.transactionCount = Integer.parseInt(scanner.nextLine()); // irrelavent count
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                if (line.isEmpty()) {
                    break;
                }
                String[] split = line.split("\t");

                ArrayList<Integer> nums = new ArrayList<>();
                int id = Integer.parseInt(split[0]);
                String[] s = split[2].split(" ");
                for (String s1 : s) {
                    nums.add(Integer.parseInt(s1));
                }
                table.table.put(id, nums);
            }
            return table;
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


}
